type input = (string, int);

type storage = {
name: string,
age: int
};
type action =
| SetName (string)
| SetAge (int)
| SetAll (input);

type returnType = (list(operation), storage);

let setName = ((s, name): (storage, string)) : returnType => ([]: list(operation), {...s, name : name});
let setAge = ((s, age): (storage, int)) : returnType => ([]: list(operation), {...s, age : age});
let setAll = ((s, (name, age)): (storage, input)) : returnType => ([]: list(operation), {...s, name: name, age: age});

let main = ((action, storage): (action, storage)) : returnType =>
    switch (action) {
        | SetName (t) => setName((storage, t))
        | SetAge (t) => setAge((storage, t))
        | SetAll (t) => setAll((storage, t))
    };
