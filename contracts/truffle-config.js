const { mnemonic, secret, password, email } = require("./faucet.json");

module.exports = {
  networks: {
    development: {
      host: "https://delphinet-tezos.giganode.io",
      port: 443, //172.15.67.251.55983',
      network_id: "*",
      secret,
      mnemonic,
      password,
      email,
      type: "tezos",
    },
  },
};
